﻿using Newtonsoft.Json;

namespace accelMotion
{
	public class BaseModel
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		public string SerializedValue()
		{
			return JsonConvert.SerializeObject(this);
		}
	}
}