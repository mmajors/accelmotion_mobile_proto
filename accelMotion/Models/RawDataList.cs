﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace accelMotion
{
	public class RawDataList : EntityBase
	{
		[JsonProperty("AttemptedExerciseId")]
		public string AttemptedExerciseId{ get; set; }

		[JsonProperty("RawDataJson")]
		public string RawDataJson{ get; set; }
	}
}