﻿namespace accelMotion
{
	public class EulerAngles
	{
		public EulerAngles (double r, double p, double y)
		{
			Roll = r;
			Pitch = p;
			Yaw = y;
		}

		public double Roll { get; set; }
		public double Pitch { get; set; }
		public double Yaw { get; set; }
	}
}