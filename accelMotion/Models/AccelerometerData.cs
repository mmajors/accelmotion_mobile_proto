﻿namespace accelMotion
{
	public class AccelerometerData
	{
		public long microSecond { get; set; }
		public float XData { get; set; }
		public float YData { get; set; }
		public float ZData { get; set; }
	}
}