﻿namespace accelMotion
{
	public class Patient : EntityBase
	{
		public string UserId { get; set; }
		public string UserName { get; set; }
		public int Age { get; set; }
		public int Height { get; set; }
		public int Weight { get; set; }
		public string TeamName { get; set; }
		public Gender Gender { get; set; }
		public Hand HandDominance { get; set; }
		public Sport Sport { get; set; }
	}
}