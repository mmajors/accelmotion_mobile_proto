﻿using Newtonsoft.Json;

namespace accelMotion
{
	public class EntityBase
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		public string SerializedValue ()
		{
			return JsonConvert.SerializeObject (this);
		}
	}
}