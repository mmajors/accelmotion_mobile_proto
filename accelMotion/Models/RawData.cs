﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace accelMotion
{
	public class RawData : EntityBase
	{
		[JsonProperty("AttemptedExerciseId")]
		public string AttemptedExerciseId{ get; set; }

		[JsonProperty("PitchValue")]
		public int PitchValue{ get; set; }

		[JsonProperty("RollValue")]
		public int RollValue{ get; set; }

		[JsonProperty("YawValue")]
		public int YawValue{ get; set; }

		public static string SerializeList (IList<RawData> entityList)
		{
			return JsonConvert.SerializeObject (entityList);
		}

		public static EntityBase GetObjectFromSerializedValue (string serializedValue)
		{
			return JsonConvert.DeserializeObject<RawData> (serializedValue);
		}

		public static IList<RawData> GetObjectsFromSerializedValue (string serializedValue)
		{
			if (serializedValue == null)
				return null;

			return JsonConvert.DeserializeObject<IList<RawData>> (serializedValue);
		}
	}
}