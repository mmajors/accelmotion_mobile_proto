﻿namespace accelMotion
{
	public class AxisDirectionStruct
	{
		public string AxisOrder = "XYZ";
		public bool NegX = false;
		public bool NegY = false;
		public bool NegZ = false;
	}
}