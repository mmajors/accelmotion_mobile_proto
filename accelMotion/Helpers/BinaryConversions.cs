﻿using System;
using System.Linq;

namespace accelMotion
{
	public static class BinaryConversions
	{
		public static float[] BinToFloat(byte[] b){
			if (b.Length % 4 != 0){
				return new float[0];
			}

			float[] returnArray = new float[b.Length / 4];

			for (int i = 0; i < returnArray.Length; i++) {
				if (BitConverter.IsLittleEndian) {
					Array.Reverse(b, i * 4, 4);
				}
				returnArray[i] = BitConverter.ToSingle(b, i * 4);
			}
			return returnArray;
		}

		public static byte[] FloatToBin(float[] f){
			byte[] returnArray = new byte[f.Length * 4];

			Buffer.BlockCopy (f, 0, returnArray, 0, returnArray.Length);

			return returnArray;
		}

		public static int[] BinToInt(byte[] b){
			if (b.Length % 4 != 0){
				return new int[0];
			}

			int[] returnArray = new int[b.Length / 4];

			for (int i = 0; i < returnArray.Length; i++) {
				if (BitConverter.IsLittleEndian) {
					Array.Reverse(b, i * 4, 4);
				}
				returnArray[i] = BitConverter.ToInt32(b, i * 4);
			}

			return returnArray;
		}

		public static short[] BinToShort(byte[] b){
			if (b.Length % 2 != 0){
				return new short[0];
			}

			short[] returnArray = new short[b.Length / 2];

			for (int i = 0; i < returnArray.Length; i++) {
				if (BitConverter.IsLittleEndian) {
					Array.Reverse(b, i * 4, 4);
				}
				returnArray[i] = BitConverter.ToInt16(b, i * 4);
			}

			return returnArray;
		}
	}
}

