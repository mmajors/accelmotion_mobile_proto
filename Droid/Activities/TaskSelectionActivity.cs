﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace accelMotion.Droid
{
	[Activity(Label = "SelectExerciseActivity")]
	public class TaskSelectionActivity : BaseActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.TaskSelection);
			SetupToolbar();
			SetTitle("Task Selection");

			_buttonShowBalanceTasksScreen = (Button)FindViewById(Resource.Id.buttonShowBalanceTasksScreen);
			_buttonShowBalanceTasksScreen.Click += DisplayBalanceTasksScreen;

			_taskSelectionListView = (ListView)FindViewById(Resource.Id.taskSelectionListView);

			List<string> items = new List<string> { "Flexibility", "Strength", "Balance", "Biomechanics", 
				"Comprehensive"};
			_taskSelectionListView.Adapter = new TaskSelectionAdapter(this, items);
		}

		private void DisplayBalanceTasksScreen(object sender, EventArgs e)
		{
			var intent = new Intent(this, typeof(BalanceTasksActivity));
			StartActivity(intent);
		}

		private Button 		_buttonShowBalanceTasksScreen;
		private ListView 	_taskSelectionListView;
	}
}