﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace accelMotion.Droid
{
	[Activity(Label = "SelectExerciseActivity")]
	public class BalanceTasksActivity : BaseActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.BalanceTasks);
			SetupToolbar();
			SetTitle("Balance Tasks");

			_buttonShowRunExerciseActivity = (Button)FindViewById(Resource.Id.buttonShowRunExerciseActivity);
			_buttonShowRunExerciseActivity.Click += DisplayRunExerciseActivity;
		}

		private void DisplayRunExerciseActivity(object sender, EventArgs e)
		{
			var intent = new Intent(this, typeof(RunExerciseActivity));
			StartActivity(intent);
		}

		private Button _buttonShowRunExerciseActivity;
	}
}