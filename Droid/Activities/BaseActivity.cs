﻿using System;
using Android.App;
using Android.Content;
using Android.Support.V4.App;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace accelMotion.Droid
{
	[Activity(Label = "BaseActivity")]
	public class BaseActivity : AppCompatActivity
	{
		public Android.Support.V7.Widget.Toolbar Toolbar { get; set; }
		public bool IncludeCalibrationMenu { get; set; }

		protected override void OnCreate(Android.OS.Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			IncludeCalibrationMenu = false;
		}

		#region Toolbar and up nav

		public void SetupToolbar()
		{
			Toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.default_toolbar);

			if (Toolbar != null)
			{
				SetSupportActionBar(Toolbar);
				SupportActionBar.SetDisplayShowTitleEnabled(false);
			}
		}

		public void SetTitle(string title)
		{
			if (Toolbar != null)
			{
				Toolbar.FindViewById<TextView>(Resource.Id.default_toolbar_title).Text = title;
			}
		}

		public void EnableUpNav()
		{
			if (Toolbar != null)
			{
				var navImage = Toolbar.FindViewById<ImageView>(Resource.Id.default_toolbar_image);
				navImage.SetImageDrawable(Resources.GetDrawable(Resource.Drawable.accel_logo));

				navImage.Click += HandleUpButtonClick;
			}
		}

		public virtual void HandleUpButtonClick(object sender, System.EventArgs e)
		{
			NavUtils.NavigateUpFromSameTask(this);
		}

		#endregion

		#region Options menu

		public override bool OnCreateOptionsMenu(IMenu menu)
		{
			if (IncludeCalibrationMenu)
			{
				MenuInflater.Inflate(Resource.Menu.main_menu, menu);
			}

			return base.OnCreateOptionsMenu(menu);
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			switch (item.ItemId)
			{
				case (Resource.Id.main_menu_calibrate):
					GoToCalibrationActivity();
					break;
				case (Resource.Id.default_toolbar_image):
					HandleUpButtonClick(null, null);
					break;
				case (Resource.Id.main_menu_bluetooth_settings):
					GoToBluetoothSettings(null, null);
					break;
				case (Resource.Id.main_menu_logout):
					PromptLogout();
					break;
			}

			return base.OnOptionsItemSelected(item);
		}

		#endregion

		public void GoToBluetoothSettings(object sender, EventArgs e)
		{
			var intent = new Intent(Android.Provider.Settings.ActionBluetoothSettings);
			StartActivity(intent);
		}

		public void GoToCalibrationActivity()
		{
			//var intent = new Intent(this, typeof(SensorCalibrationActivity));
			//StartActivity(intent);
		}

		public virtual void PromptLogout()
		{
			var alert = new Android.Support.V7.App.AlertDialog.Builder(this);

			alert.SetTitle("Logout?");
			alert.SetMessage("Are you sure you want to log out?");
			alert.SetPositiveButton("Yes", (senderAlert, args) => Logout());
			alert.SetNegativeButton("No", (senderAlert, args) => ((Android.Support.V7.App.AlertDialog)senderAlert).Dismiss());
			RunOnUiThread(() => alert.Show());
		}

		void Logout()
		{
			ApplicationState.Logout();

			Intent intent = new Intent(this, typeof(SplashActivity));
			intent.PutExtra("finish", true);
			intent.SetFlags(ActivityFlags.ClearTop |
				ActivityFlags.ClearTask |
				ActivityFlags.NewTask);
			StartActivity(intent);

			Finish();
		}
	}
}