﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Xamarin.Android;

namespace accelMotion.Droid
{
	[Activity(Label = "DashboardActivity")]
	public class DashboardActivity : BaseActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.Dashboard);

			SetupToolbar();
			SetTitle("Dashboard");

			_buttonCompleted = (Button)FindViewById(Resource.Id.buttonCompleted);
			_buttonCompleted.Click += CompleteExercise;

			SetupGraph();
		}

		private void SetupGraph()
		{
			plotView = FindViewById<PlotView>(Resource.Id.plotView);

			var plotModel = new PlotModel
			{
				Title = "DPSR Indicator",
				Subtitle = "Green = DPSR Goal; Red = Your DPSR",
				Background = OxyColors.LightYellow,
				PlotAreaBackground = OxyColors.LightGray
			};

			var categoryAxis = new CategoryAxis 
			{ 
				Position = AxisPosition.Bottom 
			};

			var valueAxis = new LinearAxis 
			{ 
				Position = AxisPosition.Left, 
				MinimumPadding = 0, 
				MajorStep = 0.25 
			};

			plotModel.Axes.Add(categoryAxis);
			plotModel.Axes.Add(valueAxis);

			var series = new ColumnSeries();

			series.Items.Add(new ColumnItem 
			{ 
				Value = 1, 
				Color = OxyColors.Green 
			});

			series.Items.Add(new ColumnItem 
			{ 
				Value = BalanceCalculations.GetDPSRValue() , 
				Color = OxyColors.Red
			});

			plotModel.Series.Add(series);

			plotView.Model = plotModel;				
		}

		private void CompleteExercise(object sender, EventArgs e)
		{
			new AlertDialog.Builder(this)
				.SetTitle("Next Steps")
				.SetMessage("What would you like to do?")
				.SetPositiveButton("Repeat Exercise", (senderAlert, args) =>
				{
					ApplicationState.ResetExercise();
					this.OnBackPressed();
				})

				.SetNegativeButton("Patient Management", (senderAlert, args) =>
				{
					var intent = new Intent(this, typeof(PatientManagementActivity));
					StartActivity(intent);
				})
			 .Show();
		}

		private Button _buttonCompleted;
		private PlotView plotView;
	}
}