﻿using System;
using System.Collections.Generic;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace accelMotion.Droid
{
	[Activity(Label = "CalibrateActivity")]
	public class CalibrateActivity : BaseActivity
	{
		ProgressDialog LoadingOverlay, ConnectionDialog;
		AlertDialog CalibrationDialog;
		volatile bool IgnorePress;
		volatile bool ShouldUpdateSensors;
		Thread T;
		Button BtnStartCalibration, BtnBluetoothSettings, BtnFinish;
		bool bCalibrationProcessComplete = false;
		private List<AccelerometerData> CalibratedData;
		long startTick;

		Sensor Sensor1;

		#region Lifecycle methods

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			IncludeCalibrationMenu = false;

			SetContentView(Resource.Layout.Calibrate);
			SetupToolbar();
			SetTitle("Calibrate");

			BtnStartCalibration = FindViewById<Button>(Resource.Id.sensor_calibration_start_btn);
			BtnFinish = FindViewById<Button>(Resource.Id.sensor_calibration_finish_btn);
			BtnBluetoothSettings = FindViewById<Button>(Resource.Id.sensor_calibration_bluetooth_settings_btn);

			//create LoadingOverlay
			LoadingOverlay = new ProgressDialog(this);
			LoadingOverlay.Indeterminate = true;
			LoadingOverlay.SetProgressStyle(ProgressDialogStyle.Spinner);
			LoadingOverlay.SetMessage(Resources.GetString(Resource.String.TaringSensors));
			LoadingOverlay.SetCancelable(false);

			CalibratedData = new List<AccelerometerData>();
		}

		protected override void OnResume()
		{
			base.OnResume();

			BtnStartCalibration.Click += StartCalibration;
			BtnFinish.Click += FinishCalibration;
			BtnBluetoothSettings.Click += GoToBluetoothSettings;
			IgnorePress = true;
		}

		protected override void OnPause()
		{
			base.OnPause();

			BtnStartCalibration.Click -= StartCalibration;
			BtnFinish.Click -= FinishCalibration;
			BtnBluetoothSettings.Click -= GoToBluetoothSettings;
		}

		#endregion

		#region Event handlers

		void StartCalibration(object sender, EventArgs e)
		{
			ConnectSensors();
		}

		void FinishCalibration(object sender, EventArgs e)
		{
			if (!bCalibrationProcessComplete)
			{
				var b = new AlertDialog.Builder(this);
				b.SetTitle("Sensor not calibrated");
				b.SetMessage("Before continuing, make sure you calibrate your sensor.");
				b.SetPositiveButton(Resource.String.OK, SensorNotCalibratedDialogDismissed);
				b.Create().Show();
				return;
			}

			var intent = new Intent(this, typeof(TaskSelectionActivity));
			ApplicationState.InitialCalibrationCompleted = true;
			StartActivity(intent);
		}

		void HandleSensorConnectionFailure()
		{
			RunOnUiThread(() =>
			{
				ConnectionDialog.Dismiss();

				var b = new AlertDialog.Builder(this);
				b.SetTitle(Resource.String.SensorConnectionError);
				b.SetMessage(Resource.String.SensorConnectionErrorMessage);
				b.SetPositiveButton(Resource.String.OK, ConnectionErrorDialogDismissed);
				b.Create().Show();
			});
		}

		public static void SensorNotCalibratedDialogDismissed(object sender, DialogClickEventArgs e)
		{ }

		public static void ConnectionErrorDialogDismissed(object sender, DialogClickEventArgs e)
		{
			Sensor.Waist = null;
		}

		#endregion

		#region Connect sensors determine which is for waist

		void ConnectSensors()
		{
			Sensor.ConnectionErrorOccurred = false;

			ConnectionDialog = new ProgressDialog(this);
			ConnectionDialog.SetTitle("One moment");
			ConnectionDialog.SetMessage("Connecting to YEI sensors..");
			ConnectionDialog.Indeterminate = true;
			ConnectionDialog.SetCancelable(false);
			ConnectionDialog.Show();

			ThreadPool.QueueUserWorkItem(async o =>
			{
				Sensor1 = new Sensor();

				Sensor1.ConnectionFailed += HandleSensorConnectionFailure;

				await Sensor1.Init().ContinueWith(async k =>
				{
					if (!Sensor.ConnectionErrorOccurred)
					{
						//request button state to clear cached value on device
						await Sensor1.GetButtonState();
						await Sensor1.SetAxisDirections("YZX", false, false, false);
						await Sensor1.SetTareCurrentOrientationAsync();

						RunOnUiThread(() =>
					   {
						   ConnectionDialog.Dismiss();
						   IgnorePress = true;
						   SetDifferenceOrientation();
					   });
					}
				});
			});
		}

		AlertDialog CreateWaistSensorDialog()
		{
			var calibrationDialogBuilder = new AlertDialog.Builder(this);
			var calibrationDialog = LayoutInflater.Inflate(Resource.Layout.CalibrationDialog, null);
			var calibrationImage = calibrationDialog.FindViewById<ImageView>(Resource.Id.calibration_image);
			calibrationImage.SetImageResource(Resource.Drawable.calibration);
			calibrationDialogBuilder.SetView(calibrationDialog);
			calibrationDialogBuilder.SetTitle("Press a button on the waist sensor");
			calibrationDialogBuilder.SetCancelable(false);
			return calibrationDialogBuilder.Create();
		}

		public void StartWaistPrompt()
		{
			CalibrationDialog = CreateWaistSensorDialog();
			CalibrationDialog.Show();

			ShouldUpdateSensors = true;

			startTick = DateTime.Now.Ticks;
			if (T == null)
			{
				T = new Thread(async () =>
				{
					while (ShouldUpdateSensors)
					{
						//int button1 = await Sensor.Waist.GetButtonState();

						//I've seen this return 70, 45, and 42 
						//even though it should only return 0, 1, 2, or 3
						//so we're gonna account for that here

						//if (button1 != 0 && button1 < 4 && !IgnorePress)
						//  For the prototype, we will only require 50 sample
						if (CalibratedData.Count == 50)
						{
							EndWaistPrompt();

							if (!ApplicationState.TEST_MODE)
							{
								ApplicationState.CalibratedData = new List<AccelerometerData>(CalibratedData);
							}
						}

						//  Continue reading the calibration
						else
						{
							ReadCalibratedData();
						}
						IgnorePress = false;
					}
				});
			}

			if (T.ThreadState == ThreadState.Unstarted)
				T.Start();
		}

		private void ReadCalibratedData()
		{
			var accelData = Sensor.Waist.ReadRawAccelerometerData();

			long endTick = DateTime.Now.Ticks;
			long tick = endTick - startTick;

			CalibratedData.Add(new AccelerometerData()
			{
				microSecond = TimeSpanHelper.GetMicroseconds(tick),
				XData = accelData.X,
				YData = accelData.Y,
				ZData = accelData.Z
			});
		}

		public void EndWaistPrompt()
		{
			ShouldUpdateSensors = false;
			if (CalibrationDialog != null)
			{
				RunOnUiThread(CalibrationDialog.Dismiss);
			}

			bCalibrationProcessComplete = true;
		}

		#endregion

		#region Set sensor offsets

		void SetDifferenceOrientation()
		{
			Sensor1.SetDifferenceQuaternionForBaseOffset();
			Sensor1.SetTareCurrentOrientation();
			Sensor.Waist.CommitSettings();

			StartWaistPrompt();
		}

		#endregion
	}
}