﻿using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.App;
using Android.Content.Res;
using System.IO;
using System.Collections.Generic;
using System;

namespace accelMotion.Droid
{
	[Activity(Label = "accelMotion", MainLauncher = true, ScreenOrientation = ScreenOrientation.Portrait, NoHistory = true)]
	public class SplashActivity : AppCompatActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.Splash);

			ShowPatientUI();
		}

		private async Task DelayPatientScreen()
		{
			await Task.Delay(Constants.SPLASH_TIMEOUT);
		}

		private async void ShowPatientUI()
		{
			await DelayPatientScreen();

			// Just here as a test to make sure we do not crash
		//	AccelNative.Initialize();

			if (ApplicationState.TEST_MODE)
			{
				ApplicationState.CalibratedData = new List<AccelerometerData>();
				ApplicationState.AccelerometerData = new List<AccelerometerData>();

				ReadTestData("pilotCAL.txt", ApplicationState.CalibratedData);
				ReadTestData("pilotDPSI1.txt", ApplicationState.AccelerometerData);
				//ReadTestData("OpenArcCalibratedTestData.txt", ApplicationState.CalibratedData);
				//ReadTestData("OpenArcBalanceTestData.txt", ApplicationState.AccelerometerData);

				BalanceCalculations.CompleteExercise(new RunExerciseActivity());
			}

			var intent = new Intent(this, typeof(PatientManagementActivity));
			StartActivity(intent);
		}

		private void ReadTestData(string filename, List<AccelerometerData> dataList)
		{
			AssetManager am = this.Assets;
			StreamReader reader = new StreamReader(am.Open(filename));

			while (!reader.EndOfStream)
			{
				var line = reader.ReadLine();
				var values = line.Split(',');

				AccelerometerData data = new AccelerometerData();
				data.microSecond = Convert.ToInt32(values[0]);
				data.XData = (float)Convert.ToDouble(values[1]);
				data.YData = (float)Convert.ToDouble(values[2]);
				data.ZData = (float)Convert.ToDouble(values[3]);

				dataList.Add(data);
			}
		}
	}
}