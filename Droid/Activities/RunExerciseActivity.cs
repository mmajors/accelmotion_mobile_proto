﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using TeleRehab.Droid;

namespace accelMotion.Droid
{
	[Activity(Label = "RunExerciseActivity")]
	public class RunExerciseActivity : BaseActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			System.GC.Collect();

			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.RunExercise);
			SetupToolbar();
			SetTitle("Dynamic Postural Stability");

			_buttonShowDashboardActivity = (Button)FindViewById(Resource.Id.buttonShowDashboardActivity);
			_buttonShowDashboardActivity.Click += DisplayDashboardActivity;

			_startStopButton = (Button)FindViewById(Resource.Id.startStopButton);
			_startStopButton.Click += StartStopButtonClick;

			_sensorUpdateService = new SensorUpdateService();
		}

		protected void OnResume()
		{
			base.OnResume();
			_startStopButton.Text = "Start";
			_exerciseStarted = false;

		}
		private void StartStopButtonClick(object sender, EventArgs e)
		{
			_exerciseStarted = !_exerciseStarted;

			if (_exerciseStarted)
			{
				_sensorUpdateService.StartUpdates(this);
				_startStopButton.Text = "Stop";
			}
			else
			{
				_sensorUpdateService.StopUpdates();
				_startStopButton.Text = "Start";
			}
		}

		public void UpdateStartButtonTextToCompleted()
		{
			_startStopButton.Text = "Exercise Completed";
		}

		public void DisplayDashboardActivity(object sender, EventArgs e)
		{
			var intent = new Intent(this, typeof(DashboardActivity));
			StartActivity(intent);
		}

		private Button 	_buttonShowDashboardActivity;
		private Button 	_startStopButton;
		private bool _exerciseStarted = false;
		private SensorUpdateService _sensorUpdateService;
	}
}