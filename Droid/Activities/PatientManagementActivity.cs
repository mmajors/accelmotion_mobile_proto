﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace accelMotion.Droid
{
	[Activity(Label = "AddPatient")]
	public class PatientManagementActivity : BaseActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.PatientManagement);
			SetupToolbar();
			SetTitle("Patient Management");

			_nextButton = (Button)FindViewById(Resource.Id.buttonShowCalibrationScreen);
			_nextButton.Click += DisplayCalibrationScreen;
		}

		private void DisplayCalibrationScreen(object sender, EventArgs e)
		{
			var intent = new Intent(this, typeof(CalibrateActivity));
			StartActivity(intent);
		}

		private Button _nextButton;
	}
}