﻿using System;
using System.Linq;

namespace accelMotion.Droid
{
	public class BalanceCalculations
	{
		private static RunExerciseActivity _activity;

		public static void CompleteExercise(RunExerciseActivity activity)
		{
			_activity = activity;

            int calibratedDataLength = ApplicationState.CalibratedData.Count * 4;
			double[] calibratedData = new double[calibratedDataLength];

			for (int i = 0; i < calibratedDataLength; i += 4)
			{
				AccelerometerData data = ApplicationState.CalibratedData.ElementAt(i / 4);
				calibratedData[i] = data.microSecond;
				calibratedData[i + 1] = data.XData;
				calibratedData[i + 2] = data.YData;
				calibratedData[i + 3] = data.ZData;
			}

            int accelerometerDataLength = ApplicationState.AccelerometerData.Count * 4;
			double[] accelerometerData = new double[accelerometerDataLength];

			for (int ii = 0; ii < accelerometerDataLength; ii += 4)
			{
				AccelerometerData data = ApplicationState.AccelerometerData.ElementAt(ii / 4);
				accelerometerData[ii] = data.microSecond;
				accelerometerData[ii + 1] = data.XData;
				accelerometerData[ii + 2] = data.YData;
				accelerometerData[ii + 3] = data.ZData;
			}

			double[] outputData = new double[1];

			AccelNative.CallCAPI(calibratedData, calibratedData.Length, accelerometerData,
								 accelerometerData.Length, outputData, new int[2]);

			_dpsrValue = outputData[0];
			Console.WriteLine("Received: " + _dpsrValue);

			_activity.DisplayDashboardActivity(null, null);
		}

		public static double GetDPSRValue()
		{
			return _dpsrValue;
		}

		private static double _dpsrValue = .95F;
	}
}