﻿using System.Runtime.InteropServices;

namespace accelMotion.Droid
{
	public static class AccelNative
	{
		[DllImport("oaaccel", EntryPoint = "accelOA_initialize")]
		public static extern void Initialize();

		[DllImport("oaaccel", EntryPoint = "CallAccelOA")]
		public static extern int CallCAPI(double[] accel_static, int static_count, double[] accel_dynamic, int dynamic_count, double[] output_data, int[] output_size);
	}
}