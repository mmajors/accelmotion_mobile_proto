using System;
using System.Collections.Generic;
using Android.App;
using Android.Runtime;
using Plugin.Connectivity;

namespace accelMotion.Droid
{
	[Application]
	public class ApplicationState : Application
	{
		public static bool TEST_MODE = false;
		public static List<AccelerometerData> AccelerometerData { get; set; }
		public static List<AccelerometerData> CalibratedData { get; set; }
		public static bool InitialCalibrationCompleted { get; set; }
		public static int CalibrationAngle { get; set; }
		public ApplicationState(IntPtr handle, JniHandleOwnership ownerShip) : base(handle, ownerShip) { }
		public static Patient CurrentPatient { get; set; }

		public override void OnCreate()
		{
			base.OnCreate();
			ApplicationState.InitialCalibrationCompleted = false;
		}

		public static void ResetExercise()
		{
			AccelerometerData?.Clear();
		}

		public static void Logout()
		{
		}

		public static bool IsConnected()
		{
			return CrossConnectivity.Current.IsConnected;
		}
	}
}