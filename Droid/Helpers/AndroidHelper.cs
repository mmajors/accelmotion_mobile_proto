﻿using System;
using Android.Content;
using Android.Views;
using Android.Views.InputMethods;

namespace accelMotion.Droid
{
	public static class AndroidHelpers
	{
		public static void ShowKeyboard(View pView, Context context)
		{
			pView.RequestFocus();

			InputMethodManager inputMethodManager = (context.ApplicationContext).GetSystemService(Context.InputMethodService) as InputMethodManager;
			inputMethodManager.ShowSoftInput(pView, ShowFlags.Forced);
			inputMethodManager.ToggleSoftInput(ShowFlags.Forced, HideSoftInputFlags.ImplicitOnly);
		}

		public static void HideKeyboard(View pView, Context context)
		{
			InputMethodManager inputMethodManager = (context.ApplicationContext).GetSystemService(Context.InputMethodService) as InputMethodManager;
			inputMethodManager.HideSoftInputFromWindow(pView.WindowToken, HideSoftInputFlags.None);
		}

		public static void EmptyDialogClickEvent(object sender, DialogClickEventArgs e) { }
	}
}