﻿using System;
using System.Collections.Generic;
using System.Threading;
using accelMotion;
using accelMotion.Droid;

namespace TeleRehab.Droid
{
	public class SensorUpdateService
	{
		private Thread T;
		private volatile bool ShouldUpdateSensors;
		private EventWaitHandle ResetSensorEvent;
		private List<AccelerometerData> AccelerometerData;
		long startTick;
		private RunExerciseActivity _activity;

		public SensorUpdateService()
		{
			AccelerometerData = new List<AccelerometerData>();
		}

		public void StartUpdates(RunExerciseActivity activity)
		{
			_activity = activity;
			ShouldUpdateSensors = true;
			Sensor.Waist.StopBatteryMonitoringThread();
			startTick = DateTime.Now.Ticks;

			if (T == null)
			{
				T = new Thread(() =>
				{
					//while (true)
					while (AccelerometerData.Count < 91)
					{
						if (!ShouldUpdateSensors)
						{
							ResetSensorEvent.WaitOne();
						}

						ReadAcccelerometerData();
					}
					StopUpdates();
				});
			}

			if (T.ThreadState == ThreadState.Unstarted)
			{
				T.Start();
			}

			if (ResetSensorEvent == null)
			{
				ResetSensorEvent = new AutoResetEvent(true);
			}
			ResetSensorEvent.Set();
		}

		private void ReadAcccelerometerData()
		{
			var accelData = Sensor.Waist.ReadRawAccelerometerData();

			long endTick = DateTime.Now.Ticks;
			long tick = endTick - startTick;

			AccelerometerData.Add(new AccelerometerData()
			{
				microSecond = TimeSpanHelper.GetMicroseconds(tick),
				XData = accelData.X,
				YData = accelData.Y,
				ZData = accelData.Z
			});
		}

		public void StopUpdates()
		{
			ShouldUpdateSensors = false;
			Sensor.Waist.StopBatteryMonitoringThread();
			ResetSensorEvent.Reset();
			if (!ApplicationState.TEST_MODE)
			{
				ApplicationState.AccelerometerData = new List<AccelerometerData>(AccelerometerData);
			}

			BalanceCalculations.CompleteExercise(_activity);
		}
	}
}