﻿using System;
using OpenTK;

namespace accelMotion.Droid
{
	public class YEIMain
	{
		YEISensorUpdateService UpdateService;

		public YEIMain ()
		{
			var calibration = new YEISensorCalibration ();
			calibration.StartCalibration ();
			calibration.CalibrationComplete += CalibrationComplete;
		}

		void CalibrationComplete ()
		{
			UpdateService = new YEISensorUpdateService ();

			var waistRotation = Sensor.Waist.GetFiltOrientQuat ();

			//  TODO
			UpdateService.InitialDifferenceQuat = CalculateDifferenceQuaternion (waistRotation, waistRotation);

			//usually run this on some sort of timer to capture data over time
			UpdateService.UpdateAngleCalculation ();
		}

		public static Quaternion CalculateDifferenceQuaternion (Quaternion thighRotation, Quaternion calfRotation)
		{
			var invertCalf = Quaternion.Invert (calfRotation);
			var difference = Quaternion.Multiply (invertCalf, thighRotation);

			difference.Normalize ();

			return difference;
		}

		public static Matrix3 CalculateDifferenceMatrixFromQuaternion (Quaternion thighRotation, Quaternion calfRotation)
		{
			var difference = CalculateDifferenceQuaternion (thighRotation, calfRotation);

			var sqw = difference.W * difference.W;
			var sqx = difference.X * difference.X;
			var sqy = difference.Y * difference.Y;
			var sqz = difference.Z * difference.Z;

			var r0c0 = (float)(sqx - sqy - sqz + sqw);
			var r1c1 = (float)(-sqx + sqy - sqz + sqw);
			var r2c2 = (float)(-sqx - sqy + sqz + sqw);

			var tmp1 = difference.X * difference.Y;
			var tmp2 = difference.Z * difference.W;
			var r1c0 = (float)2.0 * (tmp1 + tmp2);
			var r0c1 = (float)2.0 * (tmp1 - tmp2);

			tmp1 = difference.X * difference.Z;
			tmp2 = difference.Y * difference.W;
			var r2c0 = (float)2.0 * (tmp1 - tmp2);
			var r0c2 = (float)2.0 * (tmp1 + tmp2);
			tmp1 = difference.Y* difference.Z;
			tmp2 = difference.X * difference.W;
			var r2c1 = (float)2.0 * (tmp1 + tmp2);
			var r1c2 = (float)2.0 * (tmp1 - tmp2);

			var matrix = new Matrix3 (
				r0c0,r0c1,r0c2,
				r1c0,r1c1,r1c2,
				r2c0,r2c1,r2c2);

			return matrix;
		}
	}
}

