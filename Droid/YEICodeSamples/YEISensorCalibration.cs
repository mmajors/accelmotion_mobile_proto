﻿using System;

namespace accelMotion.Droid
{
	public delegate void CalibrationComplete ();

	public class YEISensorCalibration
	{
		public event CalibrationComplete CalibrationComplete;

		public void StartCalibration ()
		{
			//shows dialog to put sensors in appropriate position before calling
			//SetBaseExpectedOrientation ()
		}

		//called when the sensors are in the "expected orientation" aligned with the leg
		void SetBaseExpectedOrientaion ()
		{
			YEISensor.Waist.SetBaseOffsetQuaternionToCurrentRotation ();
		}

		//called when the sensors have been strapped on the body
		//after SetBaseExpectedOrientation is called
		void SetDifferenceOrientation ()
		{
			YEISensor.Waist.SetDifferenceQuaternionForBaseOffset ();

			YEISensor.Waist.CommitSettings ();

			CalibrationComplete?.Invoke ();
		}
	}
}

