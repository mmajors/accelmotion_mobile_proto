﻿using System;
using accelMotion;
using OpenTK;

namespace accelMotion.Droid
{
	public class YEISensorUpdateService
	{
		public Quaternion InitialDifferenceQuat;

		public void UpdateAngleCalculation ()
		{
			var rotationData = GetRawDataFromDifferenceQuat ();

			LogData (rotationData);
		}

		public void LogData (RawData rotationData)
		{
			//Log the data
		}
	}
}

