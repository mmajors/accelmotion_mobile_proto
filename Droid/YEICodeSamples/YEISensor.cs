﻿using System;
using OpenTK;
using Android.Bluetooth;
using System.IO;

namespace accelMotion.Droid
{
	public class YEISensor
	{
		public BluetoothSocket BTSocket;
		public Stream BTOutStream;
		public Stream BTInStream;
		public static YEISensor Waist;

		void Init ()
		{
			//opens BTSocket
		}

		public byte CreateChecksum(byte[] data)
		{
			byte checksum = 0;
			for(int i = 0; i < data.Length; i++){
				checksum += (byte)(data[i] % 256);
			}
			return checksum;
		}

		public void Write(byte[] data)
		{
			byte[] msgBuffer = new byte[data.Length + 2];
			Array.Copy (data, 0, msgBuffer, 1, data.Length);
			msgBuffer[0] = (byte)0xf7;
			msgBuffer[data.Length + 1] = CreateChecksum (data);

			BTOutStream.Write(msgBuffer, 0, msgBuffer.Length);
			BTOutStream.Flush();
		}

		public byte[] Read (int amnt)
		{
			byte[] response = new byte[amnt];
			int amntRead = 0;
			while (amntRead < amnt){

				if (BTInStream.IsDataAvailable ())
				{
					amntRead +=  BTInStream.Read(response, amntRead, amnt - amntRead);
				}
			}
			return response;
		}

		public Quaternion GetFiltOrientQuat()
		{
			byte[] response = null;
			byte[] send_data = {(byte)0x06};
			Write (send_data);
			response = Read (16);

			var floatArray = BinaryConversions.BinToFloat(response);

			return new Quaternion { X = floatArray[0], Y = floatArray[1], Z = floatArray[2], W = floatArray[3] };
		}

		public void SetBaseOffsetQuaternionToCurrentRotation ()
		{
			byte[] send_data = { (byte)0x16 };
			Write (send_data);
		}

		public void SetDifferenceQuaternionForBaseOffset ()
		{
			byte[] send_data = { (byte)0x13 };
			Write (send_data);
		}

		public void CommitSettings ()
		{
			byte[] send_data = { (byte)0xe1 };
			Write (send_data);
		}
	}
}

