﻿using System;
using OpenTK;

namespace accelMotion.Droid
{
	public class OASensorCalculations
	{
		public static double CalculateAngleBetweenVectors(Vector3 dynamicForward, Vector3 referenceForward, Vector3 referenceUp){
			var axis = Vector3.Cross (referenceForward, dynamicForward);

			var angle = AngleWithAtan3D (dynamicForward, referenceForward);

			var vd = Vector3.Dot (referenceUp, axis);

			if (angle*vd < 0)
			{
				angle = angle * -1;
			}

			angle = angle * (180.0/Math.PI);//convert to degrees, ya dingus

			return angle;
		}

		public static Quaternion CalculateDifferenceQuaternion (Quaternion thighRotation, Quaternion calfRotation)
		{
			var invertCalf = Quaternion.Invert (calfRotation);
			var difference = Quaternion.Multiply (invertCalf, thighRotation);

			difference.Normalize ();

			return difference;
		}

		public static Matrix3 CalculateDifferenceMatrixFromQuaternion (Quaternion thighRotation, Quaternion calfRotation)
		{
			var invertThigh = Quaternion.Invert (thighRotation);
			var difference = Quaternion.Multiply (invertThigh, calfRotation);

			difference.Normalize ();

			var sqw = difference.W * difference.W;
			var sqx = difference.X * difference.X;
			var sqy = difference.Y * difference.Y;
			var sqz = difference.Z * difference.Z;

			// invs (inverse square length) is only required if quaternion is not already normalised
			var inv = 1 / (sqx + sqy + sqz + sqw);
			var r0c0 = (float)(sqx - sqy - sqz + sqw) * inv;
			var r1c1 = (float)(-sqx + sqy - sqz + sqw) * inv;
			var r2c2 = (float)(-sqx - sqy + sqz + sqw) * inv;

			var tmp1 = difference.X * difference.Y;
			var tmp2 = difference.Z * difference.W;
			var r1c0 = (float)2.0 * (tmp1 + tmp2) * inv;
			var r0c1 = (float)2.0 * (tmp1 - tmp2) * inv;

			tmp1 = difference.X * difference.Z;
			tmp2 = difference.Y * difference.W;
			var r2c0 = (float)2.0 * (tmp1 - tmp2) * inv;
			var r0c2 = (float)2.0 * (tmp1 + tmp2) * inv;
			tmp1 = difference.Y* difference.Z;
			tmp2 = difference.X * difference.W;
			var r2c1 = (float)2.0 * (tmp1 + tmp2) * inv;
			var r1c2 = (float)2.0 * (tmp1 - tmp2) * inv;

			var matrix = new Matrix3 (
				r0c0,r0c1,r0c2,
				r1c0,r1c1,r1c2,
				r2c0,r2c1,r2c2);

			return matrix;
		}

		public static double[] CalculatePitchYawRoll (Vector3 thighForward, Vector3 thighDown, Vector3 calfForward, Vector3 calfDown)
		{
//			Assumes the devices' axis directions are default (XYZ) and are positioned or has had its
//			orientation manipulated so that the Right axis is up

//			First, calculate the right vector for both devices using the forward and down vectors
			var thighRight = Vector3.Normalize (Vector3.Cross (thighForward, thighDown));
			var calfRight = Vector3.Normalize (Vector3.Cross (calfForward, calfDown));

//			Second, calculate the angle between the right vectors and a vector perpendicular to them
			var angle = AngleWithAtan3D (calfRight, thighRight);
			var axis = Vector3.Normalize (Vector3.Cross (calfRight, thighRight));

//			Third, create a quaternion using the calculated axis and angle that will be used to transform the
//			forward vector of the second device so that it is on the same horizontal plane as the forward
//			vector of the first device
			var quat = Quaternion.FromAxisAngle(axis, (float)angle);
			var transformedCalfForward = Vector3.Normalize (Vector3.Transform (calfForward, quat));

//			Fourth, calculate the angle between the transformed forward vector and the forward vector of the
//			first device
//			This angle is the yaw
			var yaw = AngleWithAtan3D (transformedCalfForward, thighForward);

//			Fifth, calculate a vector perpendicular to the transformed forward vector and the forward vector
//			of the first device
			axis = Vector3.Normalize(Vector3.Cross(transformedCalfForward, thighForward));

//			Sixth, create a quaternion using the calculated axis and yaw angle that will be used to transform
//			the forward vector of the second device so that it is on the same vertical plane as the forward
//			vector of the first device and to transform the down vector of the second device to be used in a
//			later calculation
			quat = Quaternion.FromAxisAngle (axis, (float)yaw);
			transformedCalfForward = Vector3.Normalize (Vector3.Transform (calfForward, quat));
			var transformedCalfDown = Vector3.Normalize (Vector3.Transform (calfDown, quat));

//			Set the sign of yaw using the axis calculated and the right vector of the first device
			var vd = Vector3.Dot(axis, thighRight);
			if (yaw * vd < 0)
			{
				yaw = yaw * -1;
			}

//			Seventh, calculate the angle between the transformed forward vector and the forward vector of
//			the first device
//			This angle is the pitch
			var pitch = AngleWithAtan3D (transformedCalfForward, thighForward);

//			Eighth, calculate a vector perpendicular to the transformed forward vector and the forward vector
//			of the first device
			axis = Vector3.Normalize(Vector3.Cross(transformedCalfForward, thighForward));

//			Ninth, create a quaternion using the calculated axis and pitch angle that will be used to transform
//			the transformed down vector so that it is on the same vertical plane as the down vector of the first
//			device
			quat = Quaternion.FromAxisAngle (axis, (float)pitch);
			transformedCalfDown = Vector3.Normalize (Vector3.Transform (transformedCalfDown, quat));

//			Set the sign of pitch using the axis calculated and the down vector of the first device
			vd = Vector3.Dot (axis, thighDown);
			if (pitch * vd < 0)
			{
				pitch = pitch * -1;
			}

//			Tenth, calculate the angle between the transformed down vector and the down vector of the first
//			device
//			This angle is the roll
			var roll = AngleWithAtan3D(transformedCalfDown, thighDown);
			axis = Vector3.Normalize (Vector3.Cross (transformedCalfDown, thighDown));

//			Set the sign of roll using the axis calculated and the forward vector of the first device
			vd = Vector3.Dot (axis, thighForward);

			if (roll * vd < 0)
			{
				roll = roll * -1;
			}

			var pyr = new [] { pitch, yaw, roll };

			//convert to degrees
			for (int i = 0; i < pyr.Length; i++)
			{
				pyr[i] = pyr[i] * (180 / Math.PI);
			}

			return pyr;
		}

		static double AngleWithAtan2D (Vector3 v1, Vector3 v2)
		{
			return (Math.Atan2 (v1.Y, v1.X) - Math.Atan2 (v2.Y, v2.X));
		}

		public static double AngleWithAtan3D (Vector3 v1, Vector3 v2)
		{
			var cross = Vector3.Cross (v1, v2);
			var x = Math.Sqrt(Math.Pow(cross.X, 2) + Math.Pow(cross.Y, 2) + Math.Pow(cross.Z, 2));
			var angle = Math.Atan2 (x, Vector3.Dot(v1, v2));

			return angle;
		}
	}
}