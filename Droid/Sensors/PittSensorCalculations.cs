﻿using System;
using OpenTK;

namespace accelMotion.Droid
{
	public class PittSensorCalculations
	{		
		public static double[] tr2rpy(Matrix3 matrix)
		{
			var rpy = new double [3];

			if (Math.Abs (matrix.R0C0) < float.Epsilon && Math.Abs (matrix.R1C0) < float.Epsilon) {
				//singularity
				rpy [0] = 0;
				rpy [1] = Math.Atan2 (-matrix.R2C0, matrix.R0C0);
				rpy [2] = Math.Atan2 (-matrix.R1C2, matrix.R1C1);
			}
			else
			{
				rpy [0] = Math.Atan2 (matrix.R1C0, matrix.R0C0);

				var sp = Math.Sin (rpy [0]);
				var cp = Math.Cos (rpy [0]);

				rpy [1] = Math.Atan2 (-matrix.R2C0, cp * matrix.R0C0 + sp * matrix.R1C0);
				rpy [2] = Math.Atan2 (sp * matrix.R0C2 - cp * matrix.R1C2, cp * matrix.R1C1 - sp * matrix.R0C1);
			}

			//convert to degrees
			for (int i = 0; i < rpy.Length; i ++)
			{
				rpy [i] = rpy [i] * (180 / Math.PI);
			}

			return rpy;
		}

		//http://planning.cs.uiuc.edu/node103.html
		public static EulerAngles MatrixToEuler(Matrix3 m)
		{
			var rpy = new double [3];

			rpy [0] = Math.Atan2 (m.R1C0, m.R0C0);

			rpy [1] = Math.Atan2 (-m.R0C2, (Math.Sqrt(Math.Pow(m.R2C1, 2) + Math.Pow(m.R2C2, 2))));

			rpy [2] = Math.Atan2 (m.R2C1, m.R2C2);
			//convert to degrees
			for (int i = 0; i < rpy.Length; i ++)
			{
				rpy [i] = rpy [i] * (180 / Math.PI);
			}

			return new EulerAngles (rpy[1], rpy[2], rpy[0]);//YZX axis order
		}

		public static Matrix3 MatrixMinor (Matrix3 m)
		{
			//lol
			var r0c0 = new Matrix2 (m.R1C1, m.R1C2, m.R2C1, m.R2C2).Determinant;
			var r0c1 = new Matrix2 (m.R1C0, m.R1C2, m.R2C0, m.R2C2).Determinant;
			var r0c2 = new Matrix2 (m.R1C0, m.R1C1, m.R2C0, m.R2C1).Determinant;
			var r1c0 = new Matrix2 (m.R0C1, m.R0C2, m.R2C1, m.R2C2).Determinant;
			var r1c1 = new Matrix2 (m.R0C0, m.R0C2, m.R2C0, m.R2C2).Determinant;
			var r1c2 = new Matrix2 (m.R0C0, m.R0C1, m.R2C0, m.R2C1).Determinant;
			var r2c0 = new Matrix2 (m.R0C1, m.R0C2, m.R1C1, m.R1C2).Determinant;
			var r2c1 = new Matrix2 (m.R0C0, m.R0C2, m.R1C0, m.R1C2).Determinant;
			var r2c2 = new Matrix2 (m.R0C0, m.R0C1, m.R1C0, m.R1C1).Determinant;

			return new Matrix3 (r0c0, r0c1, r0c2, r1c0, r1c1, r1c2, r2c0, r2c1, r2c2);
		}

		public static Matrix3 MatrixOfCofactors (Matrix3 matrixMinor)
		{
			var m = matrixMinor;

			return new Matrix3 (
				m.R0C0, m.R0C1 * -1, m.R0C2,
				m.R1C0 * -1, m.R1C1, m.R1C2 * -1,
				m.R2C0, m.R2C1 * -1, m.R2C2);
		}

		public static Matrix3 InvertMatrix (Matrix3 matrix)
		{
			var minor = MatrixMinor (matrix);

			var cofactor = MatrixOfCofactors (minor);

			var adj = new Matrix3 (cofactor.R0C0, cofactor.R1C0, cofactor.R2C0,
				cofactor.R0C1, cofactor.R1C1, cofactor.R2C1,
				cofactor.R0C2, cofactor.R1C2, cofactor.R2C2);

			adj.Multiply (1 / matrix.Determinant);
			return adj;
		}
	}
}