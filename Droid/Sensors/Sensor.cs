﻿using System;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Android.Bluetooth;
using Android.Runtime;
using OpenTK;
using Java.Util.Concurrent.Locks;
using Java.Util;
using System.Threading;

namespace accelMotion.Droid
{
	public delegate void ConnectionFailed ();

	public class Sensor
	{
		public BluetoothSocket BTSocket;
		public Stream BTOutStream;
		public Stream BTInStream;
		public ReentrantLock CallLock;
		public Quaternion? RotationalOffset;
		public static ArrayList ConnectedSensors;
		public static Sensor Waist;
		public static volatile bool ConnectionErrorOccurred;
		public string Name;
		public event ConnectionFailed ConnectionFailed;
		public volatile bool ShouldMonitorBattery;
		public ILogger ErrorLogger;

		public Sensor(){
			Sensor.Waist = this;
		}

		public async Task Init ()
		{
			if (Sensor.ConnectionErrorOccurred)
			{
				return;
			}

			ShouldMonitorBattery = true;

			if (Sensor.ConnectedSensors == null)
			{
				Sensor.ConnectedSensors = new ArrayList ();
			}

			BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.DefaultAdapter;
			var deviceUUID = UUID.FromString("00001101-0000-1000-8000-00805F9B34FB");
			string serverMAC = null;

			// Get a set of currently paired devices
			var pairedDevices = mBluetoothAdapter.BondedDevices;

			// If there are paired devices, set the device mac address string as needed
			//(for now we assume the only paired device is the 3-Space sensor)
			if (pairedDevices.Count > 0) {
				foreach (var device in pairedDevices) {
					if ((device.Name.Contains(Constants.BLUETOOTH_DEVICE_NAME)) && !Sensor.ConnectedSensors.Contains(device.Name))
					{
						Console.WriteLine (device.Name);
						Name = device.Name;
						serverMAC = device.Address;
						break;
					}
				}
			}

			if (serverMAC != null){
				//Get a reference to the remote device
				BluetoothDevice remote_device = mBluetoothAdapter.GetRemoteDevice(serverMAC);

				int i = 1;
				while (!Sensor.ConnectionErrorOccurred){
					try{
						IntPtr createRfcommSocket = JNIEnv.GetMethodID (
							remote_device.Class.Handle,
							"createRfcommSocket",
							"(I)Landroid/bluetooth/BluetoothSocket;");

						BTSocket = remote_device.CreateRfcommSocketToServiceRecord(deviceUUID);
						
						//Stop discovery if it is enabled
						mBluetoothAdapter.CancelDiscovery();

						//Try to connect to the remote device.
						await BTSocket.ConnectAsync ();
						ConnectedSensors.Add (Name);

						break;
					}catch (Exception e){
						if (i > 6)
						{
							Sensor.ConnectionErrorOccurred = true;
							ConnectionFailed?.Invoke ();
							mBluetoothAdapter.CancelDiscovery ();
							return;
						}

						Console.WriteLine (Name + " CONNECTION ATTEMPT " + i + " FAILED MISERABLY " + e.ToString());
						i++;
					}
				}

				//Now lets create the in/out streams
				BTOutStream = BTSocket.OutputStream;
				BTInStream = BTSocket.InputStream;
				CallLock = new ReentrantLock();
			}
		}

		public void StartBatteryMonitoringThread ()
		{
			ShouldMonitorBattery = true;

			Task.Run (() => {
				while(ShouldMonitorBattery){
					Thread.Sleep(300);
					var b = GetBatteryLife();
				}
			});
		}

		public void StopBatteryMonitoringThread ()
		{
			ShouldMonitorBattery = false;
		}

		public Vector3 CalculateDeviceVector(Vector3 vectorDesired)
		{
			Vector3 v;

			var tared_q = GetFiltOrientQuat();
			v = Vector3.Transform(vectorDesired, tared_q);

			return v;
		}

		public async Task<Vector3> CalculateDeviceVectorAsync(Vector3 vectorDesired)
		{
			Vector3 v;

			var tared_q = await GetFiltOrientQuatAsync();
			v = Vector3.Transform(vectorDesired, tared_q);

			return v;
		}
		#region read/write/close

		public byte CreateChecksum(byte[] data){
			byte checksum = 0;
			for(int i = 0; i < data.Length; i++){
				checksum += (byte)(data[i] % 256);
			}
			return checksum;
		}

		public async Task WriteAsync(byte[] data){
			byte[] msgBuffer = new byte[data.Length + 2];
			Array.Copy (data, 0, msgBuffer, 1, data.Length);
			msgBuffer[0] = (byte)0xf7;
			msgBuffer[data.Length + 1] = CreateChecksum(data);
			try {
				await BTOutStream.WriteAsync(msgBuffer, 0, msgBuffer.Length);
				BTOutStream.Flush();
			} catch (Exception e) {
				ErrorLogger.WriteLog (e);
				Console.WriteLine ("********** BT WRITE ERR: " + e);
			}
		}

		public void Write(byte[] data){
			byte[] msgBuffer = new byte[data.Length + 2];
			Array.Copy (data, 0, msgBuffer, 1, data.Length);
			msgBuffer[0] = (byte)0xf7;
			msgBuffer[data.Length + 1] = CreateChecksum(data);
			try {
				BTOutStream.Write(msgBuffer, 0, msgBuffer.Length);
				BTOutStream.Flush();
			} catch (Exception e) {
				ErrorLogger.WriteLog (e);
				Console.WriteLine ("********** BT WRITE ERR: " + e);
			}
		}

		public async Task<byte[]> ReadAsync(int amnt){
			byte[] response = new byte[amnt];
			int amntRead = 0;
			while (amntRead < amnt){
				try {
					if(BTInStream.IsDataAvailable ())
					{
						amntRead +=  await BTInStream.ReadAsync(response, amntRead, amnt - amntRead);
					}
				}catch (Exception e) {
					ErrorLogger.WriteLog (e);
					Console.WriteLine ("********** BT READ ERR: " + e);
				}
			}
			return response;
		}

		public byte[] Read (int amnt)
		{
			byte[] response = new byte[amnt];
			int amntRead = 0;
			while (amntRead < amnt){
				try {
					if (BTInStream.IsDataAvailable ())
					{
						amntRead +=  BTInStream.Read(response, amntRead, amnt - amntRead);
					}
				}catch (Exception e) {
					ErrorLogger.WriteLog (e);
					Console.WriteLine ("********** BT READ ERR: " + e);
				}
			}
			return response;
		}

		public void Close(){
			//We are done, so lets close the connection
			try {
		
				if (BTSocket !=null){
					BTSocket.Close();
				}

			}catch (Exception e) {
				ErrorLogger.WriteLog (e);
				Console.WriteLine ("********** BT CLOSE ERR: " + e);
			}
		}

		#endregion

		#region get/set sensor data

		public async Task SetTareCurrentOrientationAsync()
		{
			byte[] send_data = {(byte)0x60};
			await WriteAsync(send_data);
		}

		public void SetTareCurrentOrientation()
		{
			byte[] send_data = {(byte)0x60};
			Write (send_data);
		}

		public async Task SetTareOrientation(byte[] orientation){
			byte[] send_data = {(byte)0x61, orientation[0], orientation[1], orientation[2], orientation[3],
				orientation[4], orientation[5], orientation[6], orientation[7], orientation[8], orientation[9],
				orientation[10], orientation[11], orientation[12], orientation[13], orientation[14], orientation[15]};
			await WriteAsync(send_data);
		}

		/// <summary>
		//  Returns the filtered, untared orientation estimate in
		//  two vector form, where the first vector refers to north
		//	and the second refers to gravity. These vectors are
		//	given in the sensor reference frame and not the
		//	global reference frame. 0x0c
		/// </summary>
		public async Task<Vector3[]> GetNorthAndGravityVectors(){
			byte[] send_data = {(byte)0x0c};
			await WriteAsync(send_data);
			byte[] response = await ReadAsync(24);

			var floatArray = BinaryConversions.BinToFloat(response);

			var northVector = new Vector3 { X = floatArray[0], Y = floatArray[1], Z = floatArray[2] };
			var gravityVector = new Vector3 { X = floatArray [3], Y = floatArray [4], Z = floatArray [5] };

			return new [] { northVector, gravityVector };
		}

		public async Task<Quaternion> GetDifferenceQuaternion ()
		{
			byte[] response = null;
			byte[] send_data = {(byte)0x05};
			await WriteAsync(send_data);
			response = await ReadAsync(16);

			var floatArray = BinaryConversions.BinToFloat(response);

			return new Quaternion { X = floatArray[0], Y = floatArray[1], Z = floatArray[2], W = floatArray[3] };
		}

		public async Task<Vector3> GetGravityVectorAsync ()
		{
			byte[] send_data = {(byte)0x22};
			await WriteAsync(send_data);
			byte[] response = await ReadAsync(12);

			var floatArray = BinaryConversions.BinToFloat(response);

			return new Vector3 { X = floatArray[0], Y = floatArray[1], Z = floatArray[2] };
		}

		public Vector3 GetGravityVector ()
		{
			byte[] send_data = {(byte)0x22};
			Write (send_data);
			byte[] response = Read (12);

			var floatArray = BinaryConversions.BinToFloat(response);

			return new Vector3 { X = floatArray[0], Y = floatArray[1], Z = floatArray[2] };
		}

		public async Task<Vector3> ReadRawAccelerometerDataAsync()
		{
			byte[] send_data = { (byte)0x42 };
			await WriteAsync(send_data);
			byte[] response = await ReadAsync(12);

			var floatArray = BinaryConversions.BinToFloat(response);

			return new Vector3 { X = floatArray[0], Y = floatArray[1], Z = floatArray[2] };
		}

		public Vector3 ReadRawAccelerometerData()
		{
			byte[] send_data = { (byte)0x42 };
			Write(send_data);
			byte[] response = Read(12);

			var floatArray = BinaryConversions.BinToFloat(response);

			return new Vector3 { X = floatArray[0], Y = floatArray[1], Z = floatArray[2] };
		}

		public async Task<Vector3[]> GetForwardAndDownVector(){
			byte[] send_data = {(byte)0x0b};
			await WriteAsync(send_data);
			byte[] response = await ReadAsync(24);

			var floatArray = BinaryConversions.BinToFloat(response);

			var forward = new Vector3 { X = floatArray[0], Y = floatArray[1], Z = floatArray[2] };
			var down = new Vector3 { X = floatArray[3], Y = floatArray[4], Z = floatArray[5] };

			return new [] { forward, down };
		}

		public async Task<Quaternion> GetFiltOrientQuatAsync()
		{
			byte[] response = null;
			byte[] send_data = {(byte)0x06};
			await WriteAsync(send_data);
			response = await ReadAsync(16);

			var floatArray = BinaryConversions.BinToFloat(response);

			return new Quaternion { X = floatArray[0], Y = floatArray[1], Z = floatArray[2], W = floatArray[3] };
		}

		public Quaternion GetFiltOrientQuat()
		{
			byte[] response = null;
			byte[] send_data = {(byte)0x06};
			Write(send_data);
			response = Read(16);

			var floatArray = BinaryConversions.BinToFloat(response);

			return new Quaternion { X = floatArray[0], Y = floatArray[1], Z = floatArray[2], W = floatArray[3] };
		}

		public async Task<Quaternion> GetFiltTaredOrientQuatAsync ()
		{
			byte[] send_data = { (byte)0x00 };
			await WriteAsync (send_data);
			var response = await ReadAsync (16);

			var floatArray = BinaryConversions.BinToFloat (response);

			return new Quaternion { X = floatArray [0], Y = floatArray [1], Z = floatArray [2], W = floatArray [3] };
		}

		public Quaternion GetFiltTaredOrientQuat ()
		{
			byte[] send_data = { (byte)0x00 };
			Write (send_data);
			var response = Read (16);

			var floatArray = BinaryConversions.BinToFloat (response);

			return new Quaternion { X = floatArray [2], Y = floatArray [0], Z = floatArray [1], W = floatArray [3] };//YZX
		}

		public async Task<Matrix3> GetUntaredOrientMatrix()
		{
			byte[] send_data = { (byte)0x08 };
			await WriteAsync (send_data);
			var response = await ReadAsync (36);

			var floatArray = BinaryConversions.BinToFloat (response);
			  
			return new Matrix3 (floatArray[0],floatArray[1],floatArray[2],
				floatArray[3], floatArray[4], floatArray[5], floatArray[6],
				floatArray[7], floatArray[8]);
		}

		public async Task<float[]> GetUntaredEulerAngles()
		{
			byte[] send_data = { (byte)0x07 };
			await WriteAsync (send_data);
			var response = await ReadAsync (12);

			var floatArray = BinaryConversions.BinToFloat (response);

			return floatArray;
		}

		public void SetBaseOffsetQuaternionToCurrentRotation ()
		{
			byte[] send_data = { (byte)0x16 };
			Write (send_data);
		}

		public void SetDifferenceQuaternionForBaseOffset ()
		{
			byte[] send_data = { (byte)0x13 };
			Write (send_data);
		}

		public void CommitSettings ()
		{
			byte[] send_data = { (byte)0xe1 };
			Write (send_data);
		}

		public Quaternion GetOffestQuaternion ()
		{
			byte[] send_data = { (byte)0x9f };
			Write (send_data);

			var response = Read (16);

			var floatArray = BinaryConversions.BinToFloat (response);

			return new Quaternion { X = floatArray [0], Y = floatArray [1], Z = floatArray [2], W = floatArray [3] };
		}

		public async Task SetAxisDirections(string axis_order, bool neg_x, bool neg_y, bool neg_z)
		{
			byte val = 0;
			if(string.Compare(axis_order, "XYZ", StringComparison.Ordinal) == 0){
				val = 0x0;
			}
			else if(string.Compare(axis_order, "XZY", StringComparison.Ordinal) == 0){
				val = 0x1;
			}
			else if(string.Compare(axis_order, "YXZ", StringComparison.Ordinal) == 0){
				val = 0x2;
			}
			else if(string.Compare(axis_order, "YZX", StringComparison.Ordinal) == 0){
				val = 0x3;
			}
			else if(string.Compare(axis_order, "ZXY", StringComparison.Ordinal) == 0){
				val = 0x4;
			}
			else if (string.Compare(axis_order, "ZYX", StringComparison.Ordinal) == 0){
				val = 0x5;
			}
			else{
				return;
			}
			if (neg_x){
				val = (byte)(val | 0x20);
			}
			if (neg_y){
				val = (byte)(val | 0x10);
			}
			if (neg_z){
				val = (byte)(val | 0x8);
			}

			byte[] send_data = {(byte)0x74, val};
			await WriteAsync(send_data);
		}

		public async Task<AxisDirectionStruct> GetAxisDirections(){
			AxisDirectionStruct axisDir = new AxisDirectionStruct();
			CallLock.Lock();
			byte[] sendData = {(byte)0x8f};
			await WriteAsync(sendData);
			byte[] response = await ReadAsync(1);
			CallLock.Unlock();
			//Determine the axis order
			int AxisOrder_num = response[0] & 7;
			if(AxisOrder_num == 0)
			{
				axisDir.AxisOrder = "XYZ";
			}
			else if(AxisOrder_num == 1)
			{
				axisDir.AxisOrder = "XZY";
			}
			else if(AxisOrder_num == 2)
			{
				axisDir.AxisOrder = "YXZ";
			}
			else if(AxisOrder_num == 3)
			{
				axisDir.AxisOrder = "YZX";
			}
			else if(AxisOrder_num == 4)
			{
				axisDir.AxisOrder = "ZXY";
			}
			else if(AxisOrder_num == 5)
			{
				axisDir.AxisOrder = "ZYX";
			}
			//Determine if any axes are negated
			if((response[0] & 0x20) > 0)
			{
				axisDir.NegX = true;
			}
			if((response[0] & 0x10) > 0)
			{
				axisDir.NegY = true;
			}
			if((response[0] & 0x8) > 0)
			{
				axisDir.NegZ = true;
			}
			return axisDir;
		}

		#endregion

		#region test/utility sensor command methods

		public async Task<string> GetSoftwareVersion(){
			CallLock.Lock();
			byte[] send_data = {(byte)0xdf};
			await WriteAsync(send_data);
			byte[] response = await ReadAsync(12);
			CallLock.Unlock();
			return Encoding.Default.GetString(response);
		}

		public async Task<string> GetHardwareVersion(){
			CallLock.Lock();
			byte[] send_data = {(byte)0xe6};
			await WriteAsync(send_data);
			byte[] response = await ReadAsync(32);
			CallLock.Unlock();
			return Encoding.Default.GetString(response);
		}

		public async Task<int> GetSerialNumber(){
			CallLock.Lock();
			byte[] send_data = {(byte)0xed};
			await WriteAsync(send_data);
			byte[] response = await ReadAsync(4);
			CallLock.Unlock();
			return BinaryConversions.BinToInt(response)[0];
		}

		public async Task SetLEDMode(int mode){
			CallLock.Lock();
			byte[] send_data = {(byte)0xc4, (byte)mode};
			await WriteAsync(send_data);
			CallLock.Unlock();
		}

		public async Task<int> GetBatteryStatus(){
			CallLock.Lock();
			byte[] send_data = {(byte)0xcb};
			await WriteAsync(send_data);
			byte[] response = await ReadAsync(1);
			CallLock.Unlock();
			return (int)response[0];
		}

		public async Task<int> GetBatteryLifeAsync(){
			byte[] send_data = {(byte)0xca};
			await WriteAsync(send_data);
			byte[] response = await ReadAsync(1);

			var x = (int)(response [0]);

			return x;
		}

		public int GetBatteryLife(){
			byte[] send_data = {(byte)0xca};
			Write(send_data);
			byte[] response = Read(1);

			var x = (int)(response [0]);

			return x;
		}

		/// <summary>
		/// Gets the confidence level of sensors
		/// </summary>
		/// <returns>Returns 1 if completely stationary, 0 if in motion, values inbetween will describe how much motion sensor is experiencing</returns>
		public async Task<float> GetConfidenceAsync(){
			CallLock.Lock ();
			byte[] send_data = { (byte)0x2d };
			await WriteAsync (send_data);
			byte[] response = await ReadAsync (4);
			CallLock.Unlock ();
			return BitConverter.ToSingle (response, 0);
		}

		/// <summary>
		/// Gets calibration mode
		/// </summary>
		/// <returns>Returns 0 for Bias, 1 for Scale-Bias, and 2 for Ortho-Calibration</returns>
		public async Task<int> GetCalibrationMode(){
			CallLock.Lock ();
			byte[] send_data = { (byte)0xaa };
			await WriteAsync (send_data);
			byte[] response = await ReadAsync (1);
			CallLock.Unlock ();
			return (int)response[0];
		}

		/// <summary>
		/// Gets button state
		/// </summary>
		/// <returns>Returns 0 for none, 1 for left pressed, 2 for right pressed, 3 for both pressed</returns>
		public async Task<int> GetButtonState(){
			//CallLock.Lock ();
			byte[] send_data = { (byte)0xfa };
			await WriteAsync (send_data);
			byte[] response = await ReadAsync (1);
			//CallLock.Unlock ();
			return (int)response[0];
		}

		public async Task SetLEDColor(float red, float green, float blue){
			CallLock.Lock();
			byte[] float_data = BinaryConversions.FloatToBin(new []{red,green,blue});
			byte[] send_data = {(byte)0xee, float_data[0], float_data[1], float_data[2], float_data[3],
				float_data[4], float_data[5], float_data[6], float_data[7],
				float_data[8], float_data[9], float_data[10], float_data[11]};
			await WriteAsync(send_data);
			CallLock.Unlock();
		}

		public async Task<float[]> GetLEDColor(){
			CallLock.Lock();
			byte[] send_data = {(byte)0xef};
			await WriteAsync(send_data);
			byte[] float_data =  await ReadAsync(12);
			CallLock.Unlock();
			return BinaryConversions.BinToFloat(float_data);
		}

		#endregion
	}
}