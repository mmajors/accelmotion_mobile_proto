﻿using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;

namespace accelMotion.Droid
{
	public class TaskSelectionAdapter : BaseAdapter<string>
	{
		Activity context;
		List<string> list;

		public TaskSelectionAdapter(Activity _context, List<string> _list)
		:base()
	{
			this.context = _context;
			this.list = _list;
		}

		public override int Count
		{
			get { return list.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override string this[int position]
		{
			get { return list[position]; }
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			if (view == null)
				view = context.LayoutInflater.Inflate(Resource.Layout.TaskSelectionListView, parent, false);

			string item = this[position];
			view.FindViewById<TextView>(Resource.Id.Title).Text = item;
			view.FindViewById<TextView>(Resource.Id.Title).TextSize = 18.0f;
			switch (position)
			{
				case 0:
					view.FindViewById<ImageView>(Resource.Id.Thumbnail).SetImageResource(Resource.Drawable.flexibility);
					break;
					
				case 1:
					view.FindViewById<ImageView>(Resource.Id.Thumbnail).SetImageResource(Resource.Drawable.strength);
					break;
					
				case 2:
					view.FindViewById<ImageView>(Resource.Id.Thumbnail).SetImageResource(Resource.Drawable.balance);
					break;
					
				case 3:
					view.FindViewById<ImageView>(Resource.Id.Thumbnail).SetImageResource(Resource.Drawable.biomechanics);
					break;
					
				case 4:
					view.FindViewById<ImageView>(Resource.Id.Thumbnail).SetImageResource(Resource.Drawable.internalrotation);
					break;
					
				default:
					view.FindViewById<ImageView>(Resource.Id.Thumbnail).SetImageResource(Resource.Drawable.flexibility);
					break;
			}

			return view;
		}
	}
}